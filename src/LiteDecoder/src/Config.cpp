#include "Config.h"

namespace Config
{
	uint16_t g_cfgTimeoutTicks = 15000;
	uint16_t g_cfgPingTicks = 2500;
	uint16_t g_cfgStateTicks = 50;
	uint16_t g_cfgCoolDownTimeoutTicks = 25;
}
